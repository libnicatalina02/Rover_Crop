from flet import *
import mysql.connector
from mysql.connector import Error

idtxt = TextField(label="Identificacion", width=300,height=50)
nametxt = TextField(label="Nombre",width=300,height=50)
lastnatxt = TextField(label="Apellido",width=300,height=50)
fnedadtxt = TextField(label="Fecha Nacimiento 'YYYY-MM-DD'",width=300,height=50)
opciones_genero = ["Masculino", "Femenino", "Otro"]
genetxt = Dropdown(label="Genero", bgcolor= "#EAF5EC",options=[dropdown.Option(opcion) for opcion in opciones_genero],width=300,height=70)
    
opciones_ocupacion = ["Estudiante", "Profesor", "Otro"]
profesitxt = Dropdown(label="profesion", bgcolor= "#EAF5EC",options=[dropdown.Option(opcion) for opcion in opciones_ocupacion],width=300,height=70)

usuariotxt = TextField(label="Crea un Usuario", width=300,height=50)
contrasenatxt = TextField(label="Crea una Contraseña", width=300,height=50, password=True, can_reveal_password=True)


conexion = mysql.connector.connect(
    host='localhost',
    port=3306,
    user='root',
    password='',
    db='rovercrop'
)

cursor = conexion.cursor()


#tabla de verificacion de los datos  

mydt = DataTable(
    columns =[
            DataColumn(Text("id",width=100)),
            DataColumn(Text("nombre")),
            DataColumn(Text("apellido")),
            DataColumn(Text("fecha_nacimiento")),
            DataColumn(Text("genero")),
            DataColumn(Text("ocupacion")),
            DataColumn(Text("ocupacion")),
            
        ], 
        rows =[]
    )


#tabla de verificacion de los datos 
mydt = DataTable(
        columns =[
            DataColumn(Text("id",width=100)),
            DataColumn(Text("nombre")),
            DataColumn(Text("apellido")),
            DataColumn(Text("fecha_nacimiento")),
            DataColumn(Text("genero")),
            DataColumn(Text("ocupacion")),
            DataColumn(Text("Acciones")),
            
            
        ], 
        rows =[]
)

#llamar base de datos requerida
def load_data():
    cursor.execute("SELECT * FROM usuario")
    result = cursor.fetchall()

    columns = [column [0] for column in cursor.description]
    rows = [dict(zip(columns, row)) for row in result]
 
    for row in rows:
            mydt.rows.append(
                DataRow(
                    cells=[
                    DataCell(Text(row['id'])),
                    DataCell(Text(row['nombre'])),
                    DataCell(Text(row['apellido'])),
                    DataCell(Text(row['fecha_nacimiento'])),
                    DataCell(Text(row['genero'])),
                    DataCell(Text(row['ocupacion'])),

                    ]
                    )
            
                )

    load_data()

#procedimiento guardar datos en la tabla 
def addtodb(e):
            try:
                sql ="INSERT INTO usuario (id,nombre, apellido, fecha_nacimiento, genero,ocupacion, nombre_usuario, contraseña) VALUES (%s,%s,%s,%s,%s,%s, %s, AES_ENCRYPT(%s, 'Yk468Fd'))"
                val =(idtxt.value,nametxt.value,lastnatxt.value,fnedadtxt.value,genetxt.value,profesitxt.value, usuariotxt.value, contrasenatxt.value)
                cursor.execute(sql, val)
                conexion.commit()
                print(cursor.rowcount, "USUARIO INGRESADO" )

                mydt.rows.clear()
                load_data()

                snack_bar = SnackBar(
                    Text("DATO AGREGADO EXITOSAMENTE", size =30),
                    bgcolor="green"
                )
                snack_bar.open = True

                
            except Exception as e:
                print(e)
                print("Error al ingresar")

            idtxt.value=""
            nametxt.value =""
            lastnatxt.value=""
            fnedadtxt.value=""
            genetxt.value =""
            profesitxt.value =""
            usuariotxt.value=""
            contrasenatxt.value=""




#clase requerida para navegacion
class Login(UserControl):
    def __init__(self, page):
        super().__init__()
        self.page = page

#pantalla inicial 
        
    def build(self):                   
      return Column(
            controls=[
                Container(
                    height=800, width=450,
                    bgcolor='black',
                    alignment=alignment.center,
                    content=Column(
                        controls=[
                            Text ('Registro', color='green', size=30),
                            idtxt,
                            nametxt,
                            lastnatxt,
                            fnedadtxt,
                            genetxt,
                            profesitxt,
                            usuariotxt,
                            contrasenatxt,
                            ElevatedButton("Guardar Registro", on_click=addtodb),
                            Container(
                                on_click=lambda _: self.page.go('/'),
                                content=Text('Vover', size=25, color='green'),
                            ),
                        ]
                    )
                )
            ]
        )