from flet import *

scroll = "always",
terrenos = ["Limoso", "Franco"]
terrenotxt = Dropdown(label="Tipo de Terreno", bgcolor= "#EAF5EC",options=[dropdown.Option(opcion) for opcion in terrenos],width=300,height=70)

cultivos = ["Zanahoria", "Papa"]
cultivotxt = Dropdown(label="Tipo de Cultivo", bgcolor= "#EAF5EC",options=[dropdown.Option(opcion) for opcion in cultivos],width=300,height=70)

estructuras = ["Oruga", "Convencional"]
estructuratxt = Dropdown(label="Tipo de Estructura", bgcolor= "#EAF5EC",options=[dropdown.Option(opcion) for opcion in estructuras],width=300,height=70)


class Simulacion(UserControl):
  def __init__(self,page):
    super().__init__()
    self.page = page
    

  def build(self):
      return Column(
        controls=[
          Container(
            height=800, width=550,
            bgcolor='black',
            alignment=alignment.center,
            #scroll = "always",
            content=Column(
              controls=[
                
                Text('Simulacion', color='green', size=30),
                
                Image(src="prueba\imagenes\limoso.PNG", width=200, height=110),
                terrenotxt,
                Image(src="prueba\imagenes\cultivo.PNG", width=200, height=110),
                cultivotxt,
                Image(src="prueba\imagenes\estructura.PNG", width=200, height=110),
                estructuratxt,

                Container(
                on_click= lambda _: self.page.go('/'), 
                content=Text('Vover',size=25,color='green'),
                  
                ),
              ]
            )
            )
          ]
      )