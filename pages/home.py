from flet import *
class Home(UserControl):
  def __init__(self,page):
    super().__init__()
    self.page = page

  def build(self):
    return Column(
      controls=[
        Container(
          height=800,width=300,
          bgcolor='black',
          alignment=alignment.center,
          content=Column(
            controls=[
              Text('RoverCrop',size=25),
              Image(src="prueba\imagenes\logo.PNG", width=150, height=150),
              
              ElevatedButton("REGISTRO", on_click= lambda _: self.page.go('/login')),
              ElevatedButton("SIMULACION", on_click= lambda _: self.page.go('/simulacion')),
                
              
            ]
          )
          )
        ]
    )